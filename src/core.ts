/// Alpha code: https://jsfiddle.net/drunkenoodle/yrwzd0m8/68/
// Built with Gulp: https://www.typescriptlang.org/docs/handbook/gulp.html
// Migration: https://www.typescriptlang.org/docs/handbook/migrating-from-javascript.html
import * as App from "./battlecontroller";

// Fire up
var b = new App.BattleController();

 // Will add options soon. Could just pick a random index, but where's the fun there? :)
b.start({
    actors: ["one", "two", "three", "four", "five", "six"],
    shuffle: true,
    mode: 0
})

function upd() {

    b.update();
    b.actionTaken();
    
    setTimeout(upd, 300);

}

document.getElementById("start").addEventListener('click', function () {

    console.log("Begin.");
    upd();

});