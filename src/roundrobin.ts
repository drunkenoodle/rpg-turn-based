import {BattleActor} from './battleactor';

// Needs improving
import {SamdaCls} from './samda';
let samda = new SamdaCls();
//

export class RoundRobin {

    currentIndex: number = 0;
    activeActor: BattleActor;
    startActor: BattleActor;
    actors: BattleActor[];

    bindActors(actorsFromParent: BattleActor[]) {

        this.actors = actorsFromParent;
        this.resetAll();

    };

    resetAll() {

        if (this.actors.length === 0) {
            console.error("No actors found.");
            return;
        }

        console.info("Refreshing all turns!");

        // Actors array in two places, happy with this?
        this.actors.forEach(function (actor: BattleActor) {
            actor.timeUntilReady = 0;
            actor.setReady();
        });

        this.activeActor = samda.first(this.actors);

        console.log(this.activeActor);

    };

    getNextReady() {

        // Hopefully this is robust enough to not have to resort to index checking.
        var lastId = this.activeActor.id;
        this.activeActor = this.actors.find(function (actor: BattleActor) {
            return actor.isReady() && actor.id !== lastId;
        }, this);

        if (!this.activeActor)
            this.resetAll();

    };

    setCurrentActorDone() {

        if (this.activeActor && this.activeActor.isReady()) {
            this.activeActor.actionTaken();
            this.getNextReady();
        }

    };

    update() {

        // ...

    };

}