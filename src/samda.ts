// My own brand of ramda (winging it) :o
export class SamdaCls {

    // Todd Motto foreach - https://github.com/toddmotto/foreach/blob/master/dist/foreach.js
    public fe(collection: any, callback: Function, scope: any) { // Wrong

        if (Object.prototype.toString.call(collection) === '[object Object]') {

            for (var prop in collection) {
                if (Object.prototype.hasOwnProperty.call(collection, prop)) {
                    callback.call(scope, collection[prop], prop, collection);
                }
            }

        } else {

            for (var i = 0, len = collection.length; i < len; i++) {
                callback.call(scope, collection[i], i, collection);
            }

        }

    }

    // Laymens chance
    flip() {
        return Math.round(Math.random());
    }

    // Custom
    same(a: any, b: any) {

        return a === b;

    }

    sameId(a: any, b: any) {

        return this.same(a, b);

    }

    valid(prop: any) {

        return this.notUndefined(prop) && this.notNull(prop);

    }

    // Undef or null checks
    notUndefined(a: any) {

        return typeof a !== 'undefined';

    }

    notNull(a: any) {

        return a !== null;

    }

    notNullOrUndefined(a: any) {

        return this.valid(a);

    }

    // Type checks
    typeString(str: string) {

        return typeof str === 'string';

    }

    // Strings
    hasLength(str: string) {

        return this.typeString(str) && str.length > 0;

    }

    notNullOrEmpty(str: string) {

        return this.typeString(str) && this.hasLength(str);

    }

    // Arrays
    first(array: any) {

        return this.notEmpty(array) ? array[0] : [];

    }

    isArray(array: any) {
        return Array.isArray(array);
    }

    notEmpty(array: any) {
        return this.isArray(array) && array.length > 0;
    }

    shuffle(array: any) {

        if (!this.notEmpty(array))
            return;

        // https://bost.ocks.org/mike/shuffle/ (wip)
        var currentIndex: number = array.length, // wrong
        temporaryValue: any, randomIndex: any;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;

    }

};