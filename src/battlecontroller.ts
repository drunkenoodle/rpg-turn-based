import { BattleActor } from './battleactor';
import { RoundRobin } from './roundrobin';

// Needs improving
import { SamdaCls } from './samda';
let samda = new SamdaCls();
//

// Needs to be centralized somewhere
enum BattleData {
    RR = 0, // Round Robin
    RRW = 1, // Round Robin with Weight
    RRA = 2, // Round Robin with Active time
    RRAW = 3 // Round Robin with Active time and Wait for current
}
//

export class BattleController {

    constructor() {
        // ...
    }

    notInitialized: boolean = true;
    waitForUserInput: boolean = false;
    actors: string[];

    currentMode: number;
    modeClass: any; // Should be type interface 'mode' or something.

    /**
     * start
     */
    public start(opts: any) { // wrong

        // Order important?
        this.registerActors(opts.actors, opts.shuffle || false);
        this.setMode(opts.mode);

    }

    // Main overseer for the fight
    setMode(type: number) {

        var pickedMode: any = null;

        // Samda is global, this needs changing
        samda.fe(BattleData, function (d: number) {
            pickedMode = samda.same(d, type) ? d : pickedMode;
        }, this);

        if (samda.notNullOrUndefined(pickedMode)) {
            this.modeClass = null;
            this.currentMode = pickedMode;
            this.setupMatch();
        } else {
            console.error("Unknown battle mode type. Set up of battle can't continue.");
        }

        return this;

    };

    setupMatch() {

        // Work in progress
        // Apply settings to actors now we have a designated mode
        if (samda.same(this.currentMode, BattleData.RR)) {

            this.modeClass = new RoundRobin();
            this.modeClass.bindActors(this.actors);

        }

    };

    // Main update method (run how you see fit such as in game loop)
    update() {

        // A signal from outside of here should trigger this bool off or on, but also from mode class
        if (this.waitForUserInput)
            return;

        this.modeClass.update();

    };

    actionTaken() {

        // Could be from AI or user input event.
        this.modeClass.setCurrentActorDone();

    };

    makeActor(id: string) {

        // http://stackoverflow.com/questions/15308371/custom-events-model-without-using-dom-events-in-javascript
        console.log("Registered:", id);

        /// Shouldn't pass anything but an ID, however it should return some sort of signal binding for the registrar to pick up.
        // Use object.create
        // sort out the callback stuff here too.
        var ba = new BattleActor(id);

        ba.TurnIsReady.on(function(item){

            console.log("Turn ready: " + item);

        });

        ba.TurnIsTaken.on(function(item){

            console.log("Turn taken: " + item);

        });

        return ba;

    };

    registerActors(arr: string[], doShuffle: boolean) {

        this.actors = arr.map(function (id) {
            return this.makeActor(id);
        }, this);

        if (doShuffle)
            this.shuffleStarter();

        return this;

    };

    shuffleStarter() {

        this.actors = samda.shuffle(this.actors);
        return this;

    };

}
