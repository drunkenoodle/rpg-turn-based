import {ITurnEvent, TurnEvent} from './TurnEvent';

// Needs improving
import {SamdaCls} from './samda';
let samda = new SamdaCls();
//

export class BattleActor {

    public id: string;
    constructor(id: string) {
        this.id = id;
    }

    // Other vars
    turnReady: boolean = false;
    waiting: boolean = false;
    timeUntilReady: number = 10;
    counter: number = 0;

    dampenTimerBy: number = 0;
    energizeTimerBy: number = 0;
    turnWeight: number = 0;

    eventTypes: any = { // Wrong.
        turnReady: 'turnReady',
        turnTaken: 'turnTaken',
        turnWeightApplied: 'turnWeightApplied'
    };

    events: string[];

    // Event fields
    private onTurnReady = new TurnEvent<void>();
    private onTurnTaken = new TurnEvent<void>();

    // Event properties // ITurnEvent??
    public get TurnIsReady(): ITurnEvent<void> { return this.onTurnReady; }
    public get TurnIsTaken(): ITurnEvent<void> { return this.onTurnTaken; }

    // Methods
    setReadyType(n: number) {
        this.timeUntilReady = n;
    }

    update() {

        // We obviously don't want to do all of this logic if we don't need a timer until ready case.
        if (this.waiting && this.timeUntilReady > 0)
            return;

        if (this.counter < this.timeUntilReady)
            this.counter += 1;

        if (this.counter >= this.timeUntilReady) {
            this.onTurnReady.trigger();
            this.waiting = true;
        }

    };

    actionTaken() {

        if (this.isReady()) {
            this.onTurnTaken.trigger();
            this.resetTurn();
        }

    };

    resetTurn() {

        this.waiting = false;
        this.turnReady = false;
        this.counter = 0;

    };

    setReady() {

        if (this.counter >= this.timeUntilReady) {
            this.waiting = true;
            this.turnReady = true;
        }

    };

    isReady() {

        return this.turnReady;

    };

}
